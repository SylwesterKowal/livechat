<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\LiveChat\Block;

use Magento\Store\Model\ScopeInterface;

class LiveChat extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context   $context,
        array                                              $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getEnabled($storeId = 0)
    {
        return $this->getConfigValue('livechat/settings/enable', $storeId);
    }

    public function getLicense($storeId = 0)
    {
        return $this->getConfigValue('livechat/settings/license', $storeId);
    }

    public function getConfigValue($field, $storeId = 0)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}

